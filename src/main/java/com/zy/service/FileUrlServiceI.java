package com.zy.service;

import com.zy.utils.ZtreeUtils;

import java.util.List;

public interface FileUrlServiceI {

    List<ZtreeUtils> getFatherFileUrls();

    List<ZtreeUtils> getSonFileUrls(String filePath);

}
