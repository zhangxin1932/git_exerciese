package com.zy.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ZtreeUtils {

    private String id;

    private String pId;

    private String name;

    private Boolean open;

    private Boolean isParent;
}
