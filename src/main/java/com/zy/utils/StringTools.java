package com.zy.utils;

import org.apache.commons.lang3.StringUtils;

public class StringTools {

    public static int getCharCount(String str, int c){
        if (StringUtils.isEmpty(str)){
            return -1;
        }
        int count = 0;
        for (int i = 0; i < str.length(); i ++){
            if (str.charAt(i) == c){
                count ++;
            }
        }
        return count;
    }
}
