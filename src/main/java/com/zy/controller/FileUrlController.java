package com.zy.controller;

import com.alibaba.fastjson.JSON;
import com.zy.service.FileUrlServiceI;
import com.zy.utils.ZtreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/files/")
public class FileUrlController {

    @Autowired
    private FileUrlServiceI fileUrlService;

    @RequestMapping("getFatherFileUrls")
    public String getFatherFileUrls(Map map){
        List<ZtreeUtils> list = fileUrlService.getFatherFileUrls();
        Object json = JSON.toJSON(list);
        map.put("fatherFileUrls", json);
        return "fileUrl";
    }

    @RequestMapping("getSonFileUrls")
    @ResponseBody
    public Object getSonFileUrls(String id){
        List<ZtreeUtils> list = fileUrlService.getSonFileUrls(id);
        Object json = JSON.toJSON(list);
        return json;
    }
}
