package com.zy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@MapperScan("com.zy.mapper")
@EnableCaching
public class SpringBootZtreeAsyncAjaxDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootZtreeAsyncAjaxDemoApplication.class, args);
    }
}
