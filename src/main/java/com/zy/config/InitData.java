package com.zy.config;

import com.zy.mapper.FileUrlMapper;
import com.zy.service.FileUrlServiceI;
import com.zy.utils.ZtreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 项目启动时加载数据的类
 */
@Component
public class InitData implements CommandLineRunner {

    @Autowired
    private FileUrlServiceI fileUrlService;

    @Override
    public void run(String... strings) throws Exception {
        getFatherFileUrls();
    }

    @CachePut(value = "fileUrls")
    public List<ZtreeUtils> getFatherFileUrls(){
        return fileUrlService.getFatherFileUrls();
    }
}
